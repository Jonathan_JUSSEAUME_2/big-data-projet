
import datetime

from api.models import Patient



class TestsApi:

     def test_bmi(self):
        patient = Patient(1, 'Ratio', 'Jojo', datetime.date(2014, 10, 9),176, 80, False, 1, 1, 1, [])
        assert round(patient.bmi(), 1) == 25.8
